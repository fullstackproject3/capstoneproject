// // // import React from 'react'
// // // import RegistrationForm from './components/Register/Register'
// // // import Login from './components/login/Login'
// // // import { Route, Routes } from 'react-router-dom'

// // // export default function AllRoutes() {
// // //   return ( 
// // //     <Routes>
// // //               <Route path="/register" element={<RegistrationForm  />} />
// // //               <Route path="/login" element={<Login/>} />


// // //     </Routes>
// // //    )}

// // // AllRoutes.jsx

// // import React from 'react';
// // import { Route, Routes } from 'react-router-dom';
// // import RegistrationForm from './components/Register/Register';
// // import Login from './components/login/Login';

// // export default function AllRoutes({ isLoggedIn, onLogin }) {
// //   return ( 
// //     <Routes>
// //       <Route path="/register" element={<RegistrationForm />} />
// //       <Route 
// //         path="/login" 
// //         element={<Login isLoggedIn={isLoggedIn} onLogin={onLogin} />} 
// //       />
// //     </Routes>
// //   );
// // }
// // AllRoutes.jsx
// import {Navigate} from 'react-router-dom'
// import React from 'react';
// import { Route, Routes } from 'react-router-dom';
// import RegistrationForm from './components/Register/Register';
// import Login from './components/login/Login';
// import HomePage from './ClientPages/ClientPage';
// import AdminDashboard from './ClientPages/AdminDashboard';
// import UserDashboard from './ClientPages/UserDashboard';

// export default function AllRoutes({ isLoggedIn, onLogin }) {
//   return ( 
//     <Routes>
//       <Route path="/" element = {<HomePage />} />
//       <Route path="/register" element={<RegistrationForm />} />
//       <Route 
//         path="/login" 
//         element={<Login isLoggedIn={isLoggedIn} onLogin={onLogin} />} 
//       />
//       <Route path="/admin" element={isLoggedIn ? <AdminDashboard /> : <Navigate to="/login" />} />
//       <Route path="/user" element={isLoggedIn ? <UserDashboard /> : <Navigate to="/login" />} />
//     </Routes>
//   );
// }

// import AdminDashboard from './components/admin/AdminDashboard';
// import UserDashboard from './components/user/UserDashboard';
//import HomePage from './ClientPages/HomePage'; 
import React from 'react';
import { Route, Routes, Navigate } from 'react-router-dom';
import Home from './components/Home';
import About from './components/About';
import Contact from './components/Contact';
import Templates from './components/Templates';
import RegistrationForm from './components/Register/Register';
import Login from './components/login/Login';
import AdminDashboard from './ClientPages/AdminDashboard';
import UserDashboard from './ClientPages/UserDashboard';

export default function AllRoutes({ isLoggedIn, onLogin }) {
  return (
    <Routes>
      <Route path="/" element={<Home />} /> {/* Default route */}
      <Route path="/about" element={<About />} />
      <Route path="/contact" element={<Contact />} />
      <Route path="/templates" element={<Templates />} />
      <Route path="/register" element={<RegistrationForm />} />
      <Route path="/login" element={<Login onLogin={onLogin} />} />
      <Route path="/admin" element={isLoggedIn ? <AdminDashboard /> : <Navigate to="/login" />} />
      <Route path="/user" element={isLoggedIn ? <UserDashboard /> : <Navigate to="/login" />} />
      <Route path="*" element={<Navigate to="/" />} /> {/* Wildcard route for non-existent paths */}
    </Routes>
  );
}

