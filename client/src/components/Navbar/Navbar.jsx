import React from "react";
import { Link } from "react-router-dom";
import './Navbar.css';

export default function Navbar({ isLoggedIn, onLogout }) {
  return (
    <div className="nav-main-container">
      <nav className="navbar">
        <div className="brand-container">
        <Link className="Brand-link" to="./Home" onClick={() => window.location.href = './Home'}>
  Talent Board
</Link>

        </div>
        <ul className="nav-links">
          <li>
            <Link to="/about">About</Link>
          </li>
          <li>
            <Link to="/templates">Templates</Link>
          </li>
          <li>
            <Link to="/contact">Contact</Link>
          </li>
          {isLoggedIn ? (
            <>
              <li>
                <Link to="/profile">Profile</Link>
              </li>
              <li>
                <button onClick={onLogout}>Logout</button>
              </li>
            </>
          ) : (
            <>
              <li>
                <Link to="/login">Login</Link>
              </li>
              <li>
                <Link to="/register">Register</Link>
              </li>
            </>
          )}
        </ul>
      </nav>
    </div>
  );
}
