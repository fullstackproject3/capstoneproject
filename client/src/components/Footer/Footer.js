import React from 'react';
import { Link } from 'react-router-dom';
import './Footer.css';

const Footer = () => {
  return (
    <footer className="footer">
      <div className="footer-content">
        <div className="footer-section about">
          
          <h4>About Us</h4>
          <p>Lorem ipsum.</p>
          <div className="contact">
            <span><i className="fas fa-envelope"></i> support@talentboard.com</span>
            <span><i className="fas fa-phone"></i> +91 976 998 55 90</span>
          </div>
        </div>

        <div className="footer-section socials">

        <h4>Social Media</h4>
          <ul>
            <li><Link to="#"><i className="fab fa-facebook"></i> Facebook</Link></li>
            <li><Link to="#"><i className="fab fa-instagram"></i> Instagram</Link></li>
            <li><Link to="#"><i className="fab fa-youtube"></i> Youtube</Link></li>
            <li><Link to="#"><i className="fab fa-twitter"></i> X (Formerly Twitter)</Link></li>
          </ul>
          </div>

        <div className="footer-section links">
          <h4>Quick Links</h4>
          <ul>
            <li><Link to="/home">Home</Link></li>
            <li><Link to="/about">About</Link></li>
            <li><Link to="/templates">Templates</Link></li>
            <li><Link to="/contact">Contact</Link></li>
          </ul>
        </div>
        {/* <div className="footer-section contact-form">
          <h4>Contact Us</h4>
          <form>
            <input type="email" name="email" className="text-input contact-input" placeholder="Your email address" />
            <textarea name="message" className="text-input contact-input" placeholder="Your message"></textarea>
            <button type="submit" className="btn btn-primary">Send</button>
          </form>
        </div> */}
      </div>
      {/* <div className="footer-bottom">
        &copy; 2024 Talent Brand. Rights reserved.
      </div> */}
    </footer>
  );
}

export default Footer;
