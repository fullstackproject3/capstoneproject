import React from 'react';
import './Contact.css';

const Contact = () => {
  return (
    <div className="contact-container">
      <div className="contact-section">
        <h2>Contact Us</h2>
        <p>If you have any questions, feedback, or inquiries,
          <p></p> please feel free to get in touch with us. 
          <p></p>We're here to help!</p>
      </div>
      
      <div className="contact-section">
        <h3>Support Email</h3>
        <p>Email: support@talentboard.com</p>
      </div>
      
      <div className="contact-section">
        <h3>Phone Number</h3>
        <p>Phone: +91 976 998 55 90</p>
      </div>
      
      <div className="contact-section">
        <h3>Address</h3>
        <p>PSR Prime Tower, Gachibowli, Hyderbad, India</p>
      </div>
      
      <div className="contact-section">
        <h3>Business Hours</h3>
        <p>Monday - Friday: 9:00 AM - 5:00 PM</p>
      </div>
    </div>
  );
}

export default Contact;
