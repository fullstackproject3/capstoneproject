
// // import React, { useState, useEffect } from 'react';
// // import './Templates.css';

// // const Templates = () => {
  
// //   const [templates, setTemplates] = useState([]);

  
// //   const dummyTemplates = [
// //     { id: 1, name: 'Template 1',  image: '/assets/template1.png', link: '/assets/template1.png' },
// //     { id: 2, name: 'Template 2',  2', image: '/assets/template2.png', link: '/assets/template2.png' },
// //     { id: 3, name: 'Template 3',  3', image: '/assets/template3.png', link: '/assets/template3.png' },
// //     { id: 4, name: 'Template 4',  4', image: '/assets/template4.png', link: '/assets/template4.png' },
// //     { id: 5, name: 'Template 5',  5', image: '/assets/template5.png', link: '/assets/template5.png' },
// //     { id: 6, name: 'Template 6',  6', image: '/assets/template6.png', link: '/assets/template6.png' }
// //   ];

// //   useEffect(() => {
    
// //     const fetchTemplates = async () => {
// //       try {
       
// //         const response = await fetch('/assets/LaTeXTemplates');
// //         if (!response.ok) {
          
// //           console.error('Failed to fetch templates, falling back to dummy data');
// //           setTemplates(dummyTemplates);
// //           return;
// //         }
// //         const data = await response.json();
        
// //         setTemplates(data);
// //       } catch (error) {
// //         console.error('Error fetching templates:', error);
// //       }
// //     };

    
// //     fetchTemplates();
// //   }, []);

// //   return (
// //     <div className="templates-container">
// //       <h2>Templates</h2>
// //       <div className="templates-grid">
// //         {/* */}
// //         {templates.map(template => (
// //           <div key={template.id} className="template-card" style={{ backgroundImage: `url(${template.image})` }}>
// //             <h3>{template.name}</h3>
// //             <p>{template.description}</p>
// //             {/*  */}
// //             <a href={template.link}>Use Template</a>
// //           </div>
// //         ))}
// //       </div>
// //     </div>
// //   );
// // }

// // export default Templates;

// // import React, { useState, useEffect } from 'react';
// import './Templates.css';

// // const dummyTemplates = [
// //   { id: 1, name: 'Template 1',  image: '/assets/template1.png', link: '/assets/template1.png' },
// //   { id: 2, name: 'Template 2',  2', image: '/assets/template2.png', link: '/assets/template2.png' },
// //   { id: 3, name: 'Template 3',  3', image: '/assets/template3.png', link: '/assets/template3.png' },
// //   { id: 4, name: 'Template 4',  4', image: '/assets/template4.png', link: '/assets/template4.png' },
// //   { id: 5, name: 'Template 5',  5', image: '/assets/template5.png', link: '/assets/template5.png' },
// //   { id: 6, name: 'Template 6',  6', image: '/assets/template6.png', link: '/assets/template6.png' }
// // ];

// // const Templates = () => {
// //   const [templates, setTemplates] = useState([]);

// //   useEffect(() => {
// //     const fetchTemplates = async () => {
// //       try {
// //         const response = await fetch('/assets/LaTeXTemplates');
// //         if (!response.ok) {
// //           console.error('Failed to fetch templates, falling back to dummy data');
// //           setTemplates(dummyTemplates);
// //           return;
// //         }
// //         const data = await response.json();
// //         setTemplates(data);
// //       } catch (error) {
// //         console.error('Error fetching templates:', error);
// //         setTemplates(dummyTemplates); // fallback to dummy data on error
// //       }
// //     };

// //     fetchTemplates();
// //   }, []);

// //   return (
// //     <div className="templates-container">
// //       <h2>Templates</h2>
// //       <div className="templates-grid">
// //         {templates.map(template => (
// //           <div key={template.id} className="template-card" style={{ backgroundImage: `url(${template.image})` }}>
// //             <h3>{template.name}</h3>
// //             <p>{template.description}</p>
// //             <a href={template.link}>Use Template</a>
// //           </div>
// //         ))}
// //       </div>
// //     </div>
// //   );
// // };
// // import {path} from '../assests/LaTeXTemplates'
// // export default Templates;
// // import { Link } from 'react-router-dom';

// // const dummyTemplates = [
// //   { id: 1, name: 'Template 1',  image: '/assets/template1.png', link: '/template1' },
// //   { id: 2, name: 'Template 2',  2', image: '/assets/template2.png', link: '/template2' },
// //   { id: 3, name: 'Template 3',  3', image: '/assets/template3.png', link: '/template3' },
// //   { id: 4, name: 'Template 4',  4', image: '/assets/template4.png', link: '/template4' },
// //   { id: 5, name: 'Template 5',  5', image: '/assets/template5.png', link: '/template5' },
// //   { id: 6, name: 'Template 6',  6', image: '/assets/template6.png', link: '/template6' }
// // ];

// // const Templates = () => {
// //   const [templates, setTemplates] = useState([]);

// //   useEffect(() => {
// //     const fetchTemplates = async () => {
// //       try {
// //         const response = await fetch('../assests/LaTeXTemplates');
// //         if (!response.ok) {
// //           console.error('Failed to fetch templates, falling back to dummy data');
// //           setTemplates(dummyTemplates);
// //           return;
// //         }
// //         const data = await response.json();
// //         setTemplates(data);
// //       } catch (error) {
// //         console.error('Error fetching templates:', error);
// //         setTemplates(dummyTemplates); // fallback to dummy data on error
// //       }
// //     };

// //     fetchTemplates();
// //   }, []);

// //   return (
// //     <div className="templates-container">
// //       <h2>Templates</h2>
// //       <div className="templates-grid">
// //         {templates.map(template => (
// //           <div key={template.id} className="template-card" style={{ backgroundImage: `url(${template.image})` }}>
// //             <h3>{template.name}</h3>
// //             <p>{template.description}</p>
// //             <Link to={template.link}>Use Template</Link>
// //           </div>
// //         ))}
// //       </div>
// //     </div>
// //   );
// // };

// // export default Templates;

// import React, { useState, useEffect } from 'react';
// import { Link } from 'react-router-dom';

// const dummyTemplates = [
//   { id: 1, name: 'Template 1',  image: require('./assets/LaTeXTemplates/template1.png').default, link: '/template1' },
//   { id: 2, name: 'Template 2',  2', image: require('../assests/LaTeXTemplates/template2.png').default, link: '/template2' },
//   { id: 3, name: 'Template 3',  3', image: require('../assests/LaTeXTemplates/template3.png').default, link: '/template3' },
//   { id: 4, name: 'Template 4',  4', image: require('../assests/LaTeXTemplates/template4.png').default, link: '/template4' },
//   { id: 5, name: 'Template 5',  5', image: require('../assests/LaTeXTemplates/template5.png').default, link: '/template5' },
//   { id: 6, name: 'Template 6',  6', image: require('../assests/LaTeXTemplates/template6.png').default, link: '/template6' }
// ];

// const Templates = () => {
//   const [templates, setTemplates] = useState([]);

//   useEffect(() => {
//     const fetchTemplates = async () => {
//       try {
//         // Simulating fetch from API, you need to provide a proper endpoint here
//         const response = await fetch('API_ENDPOINT');
//         if (!response.ok) {
//           console.error('Failed to fetch templates, falling back to dummy data');
//           setTemplates(dummyTemplates);
//           return;
//         }
//         const data = await response.json();
//         setTemplates(data);
//       } catch (error) {
//         console.error('Error fetching templates:', error);
//         setTemplates(dummyTemplates); // fallback to dummy data on error
//       }
//     };

//     fetchTemplates();
//   }, []);

//   return (
//     <div className="templates-container">
//       <h2>Templates</h2>
//       <div className="templates-grid">
//         {templates.map(template => (
//           <div key={template.id} className="template-card" style={{ backgroundImage: `url(${template.image})` }}>
//             <h3>{template.name}</h3>
//             <p>{template.description}</p>
//             <Link to={template.link}>Use Template</Link>
//           </div>
//         ))}
//       </div>
//     </div>
//   );
// };

// export default Templates;

// import React, { useState, useEffect } from 'react';
// import { Link } from 'react-router-dom';

// // Import images
// import template1 from '../assests/LaTeXTemplates/template1';
// import template2 from '../assests/LaTeXTemplates/template2';
// import template3 from '../assests/LaTeXTemplates/template3';
// import template4 from '../assests/LaTeXTemplates/template4.png';
// import template5 from '../assests/LaTeXTemplates/template5.png';
// import template6 from '../assests/LaTeXTemplates/template6.png';
// import template1 from '../assets/LaTeXTemplates/template1';
// import template2 from '../assets/LaTeXTemplates/template2';
// import template3 from '../assets/LaTeXTemplates/template3';
// import template4 from '../assets/LaTeXTemplates/template4.png';
// import template5 from '../assets/LaTeXTemplates/template5.png';
// import template6 from '../assets/LaTeXTemplates/template6.png';

// const dummyTemplates = [
//   { id: 1, name: 'Template 1',  image: template1, link: '/template1' },
//   { id: 2, name: 'Template 2',  2', image: template2, link: '/template2' },
//   { id: 3, name: 'Template 3',  3', image: template3, link: '/template3' },
//   { id: 4, name: 'Template 4',  4', image: template4, link: '/template4' },
//   { id: 5, name: 'Template 5',  5', image: template5, link: '/template5' },
//   { id: 6, name: 'Template 6',  6', image: template6, link: '/template6' }
// ];

// const Templates = () => {
//   const [templates, setTemplates] = useState([]);

//   useEffect(() => {
//     const fetchTemplates = async () => {
//       try {
//         // Simulating fetch from API, you need to provide a proper endpoint here
//         const response = await fetch('API_ENDPOINT');
//         if (!response.ok) {
//           console.error('Failed to fetch templates, falling back to dummy data');
//           setTemplates(dummyTemplates);
//           return;
//         }
//         const data = await response.json();
//         setTemplates(data);
//       } catch (error) {
//         console.error('Error fetching templates:', error);
//         setTemplates(dummyTemplates); // fallback to dummy data on error
//       }
//     };

//     fetchTemplates();
//   }, []);

//   return (
//     <div className="templates-container">
//       <h2>Templates</h2>
//       <div className="templates-grid">
//         {templates.map(template => (
//           <div key={template.id} className="template-card" style={{ backgroundImage: `url(${template.image})` }}>
//             <h3>{template.name}</h3>
//             <p>{template.description}</p>
//             <Link to={template.link}>Use Template</Link>
//           </div>
//         ))}
//       </div>
//     </div>
//   );
// };

// export default Templates;

import React, { useState, useEffect } from 'react';
//import { Link } from 'react-router-dom';

import './Templates.css';
// Import images
import template1 from '../assets/LaTeXTemplates/template1.png';
import template2 from '../assets/LaTeXTemplates/template2.png';
import template3 from '../assets/LaTeXTemplates/template3.png';
import template4 from '../assets/LaTeXTemplates/template4.png';
import template5 from '../assets/LaTeXTemplates/template5.png';
import template6 from '../assets/LaTeXTemplates/template6.png';

const Templates = () => {
  const [templates, setTemplates] = useState([]);
// eslint-disable-next-line
  const dummyTemplates = [
    { id: 1, name: 'Template 1', image: template1, link: '/assets/template1.png' },
    { id: 2, name: 'Template 2', image: template2, link: '/assets/template2.png' },
    { id: 3, name: 'Template 3', image: template3, link: '/assets/template3.png' },
    { id: 4, name: 'Template 4', image: template4, link: '/assets/template4.png' },
    { id: 5, name: 'Template 5', image: template5, link: '/assets/template5.png' },
    { id: 6, name: 'Template 6', image: template6, link: '/assets/template6.png' }
  ];

  useEffect(() => {
    const fetchTemplates = async () => {
      try {
        const response = await fetch('/assets/LaTeXTemplates');
        if (!response.ok) {
          console.error('Failed to fetch templates, falling back to dummy data');
          setTemplates(dummyTemplates);
          return;
        }
        const data = await response.json();
        setTemplates(data);
      } catch (error) {
        console.error('Error fetching templates:', error);
      }
    };

    fetchTemplates();
  }, [dummyTemplates]);

  return (
<div className="templates-container">
  <h2>Templates</h2>
  <div className="templates-grid">
    {templates.map(template => (
      <div key={template.id} className="template-card" style={{ backgroundImage: `url(${template.image})` }}>
        {/* <h3>Template {template.id}</h3> */}
        {/* <p>{template.description}</p> */}
        <a href={template.link}>Proceed</a>
      </div>
    ))}
  </div>
</div>

  );
};

export default Templates;