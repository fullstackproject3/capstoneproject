
// // // // import React, { useState } from 'react';
// // // // import axios from 'axios';
// // // // import './Login.css';
// // // // import Alert from '../../Alerts/Alert';

// // // // const Login = () => {
// // // //     const [formData, setFormData] = useState({
// // // //         username: '',
// // // //         password: ''
// // // //     });
// // // //     const [alert, setAlert] = useState(null);

// // // //     const handleCloseAlert = () => {
// // // //         setAlert(null);
// // // //     };

// // // //     const handleChange = (e) => {
// // // //         setFormData({
// // // //             ...formData,
// // // //             [e.target.name]: e.target.value
// // // //         });
// // // //     };

// // // //     const handleSubmit = async (e) => {
// // // //         e.preventDefault();
// // // //         try {
// // // //             const response = await axios.post('/api/login', formData); 
// // // //             console.log('User logged in successfully:', response.data);
// // // //             setAlert({ message: 'Login Success', type: 'success' });
// // // //             // Redirect or perform further actions upon successful login
// // // //         } catch (error) {
// // // //             console.error('Error logging in:', error.response ? error.response.data : error.message);
// // // //             setAlert({ message: error.response ? error.response.data : error.message, type: 'error' });
// // // //         }
// // // //     };

// // // //     return (
// // // //         <div className="login-container">
// // // //             {alert && <Alert message={alert.message} type={alert.type} onClose={handleCloseAlert} />}
// // // //             <form className="login-form" onSubmit={handleSubmit}>
// // // //                 <h2>Login</h2>
// // // //                 <div className="form-group">
// // // //                     <label htmlFor="username">Username:</label>
// // // //                     <input type="text" id="username" name="username" value={formData.username} onChange={handleChange} required />
// // // //                 </div>
// // // //                 <div className="form-group">
// // // //                     <label htmlFor="password">Password:</label>
// // // //                     <input type="password" id="password" name="password" value={formData.password} onChange={handleChange} required />
// // // //                 </div>
// // // //                 <button className="submit-button" type="submit">Login</button>
// // // //             </form>
// // // //             <div className="additional-options">
// // // //                 <a href="/register">Register</a>
// // // //                 <a href="/forgot-password">Forgot Password?</a>
// // // //             </div>
// // // //         </div>
// // // //     );
// // // // };

// // // // export default Login;

// // // // Login.js

// // // import React, { useState } from 'react';
// // // import axios from 'axios';
// // // import { useHistory } from 'react-router-dom'; // Import useHistory hook
// // // import Alert from './Alert';
// // // import './Login.css';

// // // const Login = () => {
// // //     const [formData, setFormData] = useState({
// // //         username: '',
// // //         password: ''
// // //     });
// // //     const [alert, setAlert] = useState(null);
// // //     const history = useHistory(); // Initialize useHistory hook

// // //     const handleCloseAlert = () => {
// // //         setAlert(null);
// // //     };

// // //     const handleChange = (e) => {
// // //         setFormData({
// // //             ...formData,
// // //             [e.target.name]: e.target.value
// // //         });
// // //     };

// // //     const handleSubmit = async (e) => {
// // //         e.preventDefault();
// // //         try {
// // //             const response = await axios.post('/api/login', formData); 
// // //             console.log('User logged in successfully:', response.data);
// // //             // Redirect based on user role
// // //             const homePageUrl = determineHomePageUrl(response.data.role); // Assuming role is returned from server
// // //             history.push(homePageUrl); // Redirect to home page
// // //         } catch (error) {
// // //             console.error('Error logging in:', error.response ? error.response.data : error.message);
// // //             setAlert({ message: error.response ? error.response.data : error.message, type: 'error' });
// // //         }
// // //     };

// // //     const determineHomePageUrl = (role) => {
// // //         // Determine and return home page URL based on user role
// // //         switch (role) {
// // //             case 'admin':
// // //                 return '/admin';
// // //             case 'user':
// // //                 return '/user';
// // //             default:
// // //                 return '/';
// // //         }
// // //     };

// // //     return (
// // //         <div className="login-container">
// // //             {alert && <Alert message={alert.message} type={alert.type} onClose={handleCloseAlert} />}
// // //             <form className="login-form" onSubmit={handleSubmit}>
// // //                 <h2>Login</h2>
// // //                 <div className="form-group">
// // //                     <label htmlFor="username">Username:</label>
// // //                     <input type="text" id="username" name="username" value={formData.username} onChange={handleChange} required />
// // //                 </div>
// // //                 <div className="form-group">
// // //                     <label htmlFor="password">Password:</label>
// // //                     <input type="password" id="password" name="password" value={formData.password} onChange={handleChange} required />
// // //                 </div>
// // //                 <button className="submit-button" type="submit">Login</button>
// // //             </form>
// // //             <div className="additional-options">
// // //                 <a href="/register">Register</a>
// // //                 <a href="/forgot-password">Forgot Password?</a>
// // //             </div>
// // //         </div>
// // //     );
// // // };

// // // export default Login;

// // // Login.js

// // // import React, { useState } from 'react';
// // // import axios from 'axios';
// // // import { useNavigate } from 'react-router-dom'; // Import useNavigate hook
// // // import Alert from '../../Alerts/Alert';
// // // import './Login.css';

// // // const Login = () => {
// // //     const [formData, setFormData] = useState({
// // //         username: '',
// // //         password: ''
// // //     });
// // //     const [alert, setAlert] = useState(null);
// // //     const navigate = useNavigate(); // Initialize useNavigate hook

// // //     const handleCloseAlert = () => {
// // //         setAlert(null);
// // //     };

// // //     const handleChange = (e) => {
// // //         setFormData({
// // //             ...formData,
// // //             [e.target.name]: e.target.value
// // //         });
// // //     };

// // //     const handleSubmit = async (e) => {
// // //         e.preventDefault();
// // //         try {
// // //             const response = await axios.post('/api/login', formData); 
// // //             console.log('User logged in successfully:', response.data);
// // //             // Redirect based on user role
// // //             const homePageUrl = determineHomePageUrl(response.data.role); // Assuming role is returned from server
// // //             navigate(homePageUrl); // Redirect to home page
// // //         } catch (error) {
// // //             console.error('Error logging in:', error.response ? error.response.data : error.message);
// // //             setAlert({ message: error.response ? error.response.data : error.message, type: 'error' });
// // //         }
// // //     };

// // //     const determineHomePageUrl = (role) => {
// // //         // Determine and return home page URL based on user role
// // //         switch (role) {
// // //             case 'admin':
// // //                 return '/admin';
// // //             case 'user':
// // //                 return '/user';
// // //             default:
// // //                 return '/';
// // //         }
// // //     };

// // //     return (
// // //         <div className="login-container">
// // //             {alert && <Alert message={alert.message} type={alert.type} onClose={handleCloseAlert} />}
// // //             <form className="login-form" onSubmit={handleSubmit}>
// // //                 <h2>Login</h2>
// // //                 <div className="form-group">
// // //                     <label htmlFor="username">Username:</label>
// // //                     <input type="text" id="username" name="username" value={formData.username} onChange={handleChange} required />
// // //                 </div>
// // //                 <div className="form-group">
// // //                     <label htmlFor="password">Password:</label>
// // //                     <input type="password" id="password" name="password" value={formData.password} onChange={handleChange} required />
// // //                 </div>
// // //                 <button className="submit-button" type="submit">Login</button>
// // //             </form>
// // //             <div className="additional-options">
// // //                 <a href="/register">Register</a>
// // //                 <a href="/forgot-password">Forgot Password?</a>
// // //             </div>
// // //         </div>
// // //     );
// // // };

// // // export default Login;


// // // import React, { useState } from 'react';
// // // import axios from 'axios';
// // // import { useNavigate } from 'react-router-dom';
// // // import Alert from '../../Alerts/Alert';
// // // import './Login.css';

// // // const Login = ({ onLogin }) => {
// // //     const [formData, setFormData] = useState({
// // //         username: '',
// // //         password: ''
// // //     });
// // //     const [alert, setAlert] = useState(null);
// // //     const navigate = useNavigate();

// // //     const handleCloseAlert = () => {
// // //         setAlert(null);
// // //     };

// // //     const handleChange = (e) => {
// // //         setFormData({
// // //             ...formData,
// // //             [e.target.name]: e.target.value
// // //         });
// // //     };

// // //     const handleSubmit = async (e) => {
// // //         e.preventDefault();
// // //         try {
// // //             const response = await axios.post('/api/login', formData);
// // //             console.log('User logged in successfully:', response.data);
// // //             onLogin(); // Update the login state in App
// // //             // Redirect based on user role
// // //             const homePageUrl = determineHomePageUrl(response.data.role);
// // //             navigate(homePageUrl); // Redirect to home page
// // //         } catch (error) {
// // //             console.error('Error logging in:', error.response ? error.response.data : error.message);
// // //             setAlert({ message: error.response ? error.response.data : error.message, type: 'error' });
// // //         }
// // //     };

// // //     const determineHomePageUrl = (role) => {
// // //         switch (role) {
// // //             case 'admin':
// // //                 return '/admin';
// // //             case 'user':
// // //                 return '/user';
// // //             default:
// // //                 return '/';
// // //         }
// // //     };

// // //     return (
// // //         <div className="login-container">
// // //             {alert && <Alert message={alert.message} type={alert.type} onClose={handleCloseAlert} />}
// // //             <form className="login-form" onSubmit={handleSubmit}>
// // //                 <h2>Login</h2>
// // //                 <div className="form-group">
// // //                     <label htmlFor="username">Username:</label>
// // //                     <input type="text" id="username" name="username" value={formData.username} onChange={handleChange} required />
// // //                 </div>
// // //                 <div className="form-group">
// // //                     <label htmlFor="password">Password:</label>
// // //                     <input type="password" id="password" name="password" value={formData.password} onChange={handleChange} required />
// // //                 </div>
// // //                 <button className="submit-button" type="submit">Login</button>
// // //             </form>
// // //             <div className="additional-options">
// // //                 <a href="/register">Register</a>
// // //                 <a href="/forgot-password">Forgot Password?</a>
// // //             </div>
// // //         </div>
// // //     );
// // // };

// // // export default Login;


// // import React, { useState } from 'react';
// // import axios from 'axios';
// // import { useNavigate } from 'react-router-dom';
// // import Alert from '../../Alerts/Alert';
// // import './Login.css';

// // const Login = ({ onLogin }) => {
// //     const [formData, setFormData] = useState({
// //         username: '',
// //         password: ''
// //     });
// //     const [loginType, setLoginType] = useState('user'); // user or admin
// //     const [alert, setAlert] = useState(null);
// //     const navigate = useNavigate();

// //     const handleCloseAlert = () => {
// //         setAlert(null);
// //     };

// //     const handleChange = (e) => {
// //         setFormData({
// //             ...formData,
// //             [e.target.name]: e.target.value
// //         });
// //     };

// //     const handleLoginTypeChange = (e) => {
// //         setLoginType(e.target.value);
// //     };

// //     const handleSubmit = async (e) => {
// //         e.preventDefault();
// //         const endpoint = loginType === 'admin' ? '/api/admin/login' : '/api/login';
// //         try {
// //             const response = await axios.post(endpoint, formData);
// //             console.log('User logged in successfully:', response.data);
// //             onLogin(); // Update the login state in App
// //             const homePageUrl = loginType === 'admin' ? '/admin' : '/user';
// //             navigate(homePageUrl); // Redirect to home page based on login type
// //         } catch (error) {
// //             console.error('Error logging in:', error.response ? error.response.data : error.message);
// //             setAlert({ message: error.response ? error.response.data : error.message, type: 'error' });
// //         }
// //     };

// //     return (
// //         <div className="login-container">
// //             {alert && <Alert message={alert.message} type={alert.type} onClose={handleCloseAlert} />}
// //             <form className="login-form" onSubmit={handleSubmit}>
// //                 <h2>Login</h2>
// //                 <div className="form-group">
// //                     <label htmlFor="username">Username:</label>
// //                     <input type="text" id="username" name="username" value={formData.username} onChange={handleChange} required />
// //                 </div>
// //                 <div className="form-group">
// //                     <label htmlFor="password">Password:</label>
// //                     <input type="password" id="password" name="password" value={formData.password} onChange={handleChange} required />
// //                 </div>
// //                 <div className="form-group">
// //                     <label htmlFor="loginType">Login as:</label>
// //                     <select id="loginType" value={loginType} onChange={handleLoginTypeChange}>
// //                         <option value="user">User</option>
// //                         <option value="admin">Admin</option>
// //                     </select>
// //                 </div>
// //                 <button className="submit-button" type="submit">Login</button>
// //             </form>
// //             <div className="additional-options">
// //                 <a href="/register">Register</a>
// //                 <a href="/forgot-password">Forgot Password?</a>
// //             </div>
// //         </div>
// //     );
// // };

// // export default Login;

// import React, { useState } from 'react';
// import axios from 'axios';
// import { useNavigate } from 'react-router-dom';
// import Alert from '../../Alerts/Alert';
// import './Login.css';

// const Login = ({ onLogin }) => {
//     const [formData, setFormData] = useState({
//         username: '',
//         password: ''
//     });
//     const [loginType, setLoginType] = useState('user'); // user or admin
//     const [alert, setAlert] = useState(null);
//     const navigate = useNavigate();

//     const handleCloseAlert = () => {
//         setAlert(null);
//     };

//     const handleChange = (e) => {
//         setFormData({
//             ...formData,
//             [e.target.name]: e.target.value
//         });
//     };

//     const handleLoginTypeChange = (e) => {
//         setLoginType(e.target.value);
//     };

//     const handleSubmit = async (e) => {
//         e.preventDefault();
//         const endpoint = loginType === 'admin' ? '/api/admin/login' : '/api/login';
//         try {
//             const response = await axios.post(endpoint, formData);
//             console.log('User logged in successfully:', response.data);
//             onLogin(); // Update the login state in App
//             const homePageUrl = loginType === 'admin' ? '/admin' : '/user';
//             navigate(homePageUrl); // Redirect to home page based on login type
//         } catch (error) {
//             console.error('Error logging in:', error.response ? error.response.data : error.message);
//             setAlert({ message: error.response ? error.response.data : error.message, type: 'error' });
//         }
//     };

//     return (
//         <div className="login-container">
//             {alert && <Alert message={alert.message} type={alert.type} onClose={handleCloseAlert} />}
//             <form className="login-form" onSubmit={handleSubmit}>
//                 <h2>Login</h2>
//                 <div className="form-group">
//                     <label htmlFor="username">Username:</label>
//                     <input type="text" id="username" name="username" value={formData.username} onChange={handleChange} required />
//                 </div>
//                 <div className="form-group">
//                     <label htmlFor="password">Password:</label>
//                     <input type="password" id="password" name="password" value={formData.password} onChange={handleChange} required />
//                 </div>
//                 <div className="form-group">
//                     <label htmlFor="loginType">Login as:</label>
//                     <select id="loginType" value={loginType} onChange={handleLoginTypeChange}>
//                         <option value="user">User</option>
//                         <option value="admin">Admin</option>
//                     </select>
//                 </div>
//                 <button className="submit-button" type="submit">Login</button>
//             </form>
//             <div className="additional-options">
//                 <a href="/register">Register</a>
//                 <a href="/forgot-password">Forgot Password?</a>
//             </div>
//         </div>
//     );
// };

// export default Login;

import React, { useState } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import Alert from '../../Alerts/Alert';
import './Login.css';

const Login = ({ onLogin }) => {
    const [formData, setFormData] = useState({
        username: '',
        password: ''
    });
    const [loginType, setLoginType] = useState('user');
    const [alert, setAlert] = useState(null);
    const navigate = useNavigate();

    const handleCloseAlert = () => {
        setAlert(null);
    };

    const handleChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value
        });
    };

    const handleLoginTypeChange = (e) => {
        setLoginType(e.target.value);
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        const endpoint = loginType === 'admin' ? '/api/admin/login' : '/api/login';
        try {
            const response = await axios.post(endpoint, formData);
            console.log('User logged in successfully:', response.data);
            localStorage.setItem('token', response.data.token); 
            onLogin();
            const homePageUrl = loginType === 'admin' ? '/admin' : '/user';
            navigate(homePageUrl); 
        } catch (error) {
            console.error('Error logging in:', error.response ? error.response.data : error.message);
            setAlert({ message: error.response ? error.response.data : error.message, type: 'error' });
        }
    };

    return (
        <div className="login-container">
            {alert && <Alert message={alert.message} type={alert.type} onClose={handleCloseAlert} />}
            <form className="login-form" onSubmit={handleSubmit}>
                <h2>Login</h2>
                <div className="form-group">
                    <label htmlFor="username">Username:</label>
                    <input type="text" id="username" name="username" value={formData.username} onChange={handleChange} required />
                </div>
                <div className="form-group">
                    <label htmlFor="password">Password:</label>
                    <input type="password" id="password" name="password" value={formData.password} onChange={handleChange} required />
                </div>
                <div className="form-group">
                    <label htmlFor="loginType">Login as:</label>
                    <select id="loginType" value={loginType} onChange={handleLoginTypeChange}>
                        <option value="user">User</option>
                        <option value="admin">Admin</option>
                    </select>
                </div>
                <button className="submit-button" type="submit">Login</button>
            </form>
            <div className="additional-options">
                <a href="/register">Register</a>
                <a href="/forgot-password">Forgot Password?</a>
            </div>
        </div>
    );
};

export default Login;
