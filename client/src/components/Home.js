
// import React from 'react';
// import { Link } from 'react-router-dom';
// import './Home.css';


// // export default Home;
// const Home = () => {
//   return (
//     <div className="home-container">
//       <h1>Welcome to Talent Board</h1>
//       <section>
//         <p>
//           Talent Board is a platform where you can create, customize, and manage your resumes with ease.
//           It is your one-stop solution for creating professional resumes that stand out. Whether you're a seasoned professional or just starting your career journey, we've got you covered.
//         </p>
//         <p>
//           With Talent Board, you can choose from a wide range of customizable templates tailored to various industries and job roles. Our intuitive interface makes it easy to create and update your resume, allowing you to focus on what matters most - showcasing your skills and experiences.
//         </p>
//         <h2>Why Choose Talent Board?</h2>
//         <ul>
//           <li>Save and edit: Save your progress and come back to edit your resume at any time.</li>
//           <li>Easy-to-use interface: Our user-friendly platform makes it simple to create and customize your resume.</li>
//           <li>Professional templates: Choose from a diverse selection of modern and professionally-designed templates.</li>
//           <li>Customization options: Tailor your resume to suit your unique style and preferences with customizable layouts and fonts.</li>
//         </ul>
//         <h2>Get Started Today</h2>
//         <p>
//           Ready to create your professional resume? Sign up for a free account or log in to get started.
//         </p>
//         <p>If you have any questions or need assistance, our support team is here to help.</p>
//         <p>Join Talent Board today and take the first step towards landing your dream job!</p>
//         <p>
//           Get started by creating an account <Link to="/register">here</Link> or if you are an account holder, login <Link to="/login">here</Link> to access our templates and create your professional resume today!
//         </p>
//         <p>
//           If you have any questions or need assistance, feel free to <Link to="/contact">contact us</Link>.
//         </p>
//       </section>
//     </div>
//   );
// }

// export default Home;

import React from 'react';
import { Link } from 'react-router-dom';
import './Home.css';
import LaTeXimage from '../assets/LaTeXTemplates/LaTeX_korekta.jpg';

const Home = () => {
  return (
    <div className="home-container">
      <h1>Welcome to Talent Board</h1>

      <div className="content">
        <div className="text">
          <p>This is where crafting standout resumes is effortless.</p>
          <p>Save time with customizable templates for all industries.</p>
          <p>Our intuitive interface streamlines editing.</p>
          <p>Choose from professional designs and personalize your resume effortlessly.</p>
          <p>
            <Link to="/register">Register</Link> or <Link to="/login">Log in</Link> now to access our templates and kickstart your journey to your dream job!
          </p>
        </div>
        <div className="image">
          <img src={LaTeXimage} alt="Resume Builder" />
        </div>
      </div>
    </div>
  );
};

export default Home;


      {/* <div className="card">
>>>>>>> upgrades
      <p>Talent Board: Your go-to platform for crafting standout resumes effortlessly. With customizable templates for all industries and career levels, 
       <p></p> our intuitive interface streamlines the creation and updating process. Focus on showcasing your skills and experiences while we handle the rest.</p>
       </div>
      <div className="card">
        <h2>Why Choose Talent Board?</h2>
        <ul>
          <li>Save and Edit: Seamlessly save your progress and return to edit your resume whenever needed.</li>
          <li>Easy-to-Use Interface: Our intuitive platform simplifies the resume creation and customization process.</li>
          <li>Professional Templates: Select from a varied array of contemporary, professionally-designed templates.</li>
          <li>Customization: Personalize your resume with adjustable layouts and fonts to reflect your individual style and preferences.</li>
        </ul>
      </div>
      <div className="card">
        <h2>Get Started Today</h2>
        <p>
          Ready to create your professional resume? Sign up for a free account or log in to get started.
        </p>
        <p>If you have any questions or need assistance, our support team is here to help.</p>
        <p>Join Talent Board today and take the first step towards landing your dream job!</p>
        <p>
          Get started by <a href="/register">registering</a> or <a href="/login">logging in</a> to access our templates and create your professional resume today!
        </p>
        <p>
          If you have any questions or need assistance, feel free to <a href="/contact">contact us</a>.
        </p>

        <p>
        Welcome to Talent Board – your premier platform for crafting standout resumes effortlessly. 
        </p>
        <p>With customizable templates tailored to all industries and career levels, our user-friendly interface simplifies the resume creation process. 
        </p>
        <p>Why choose Talent Board? Save time with seamless progress saving and editing. 
        </p>
        <p>Enjoy an intuitive interface that streamlines customization. 
        </p>
        <p>Select from a diverse range of professionally-designed templates. 
        </p>
        <p>Personalize your resume with adjustable layouts and fonts. 
        </p>
        <p>Ready to kickstart your professional journey? Sign up or log in now to access our templates and create your dream resume. 
        </p>
        <p>Need assistance? Our support team is here to help. 
        </p>
        <p>Join Talent Board today and land your dream job!
        </p> 
      </div>*/}

