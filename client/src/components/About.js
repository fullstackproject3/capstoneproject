import React from 'react';
import './About.css';

const About = () => {
  return (
    <div className="about-container">
      <h2>About Us</h2>
      <p>Welcome to our platform, where we aim to revolutionize the way people create resumes!</p>
      
      <h3>Resume Builder</h3>
      <p>Our Resume Builder is designed to make the resume creation process easy, efficient, and professional. </p>
        <p>With our intuitive interface and customizable templates, you can create a standout resume in minutes.</p>
      
      <h3>Meet the Authors</h3>
      <div className="about-author">
        <h4>Y.T.S</h4>
        <p>Tri is a passionate M.L. engineer. He specializes in frontend, ML technologies and loves creating user-friendly interfaces.</p>
      </div>
      <div className="about-author">
        <h4>S.R.V</h4>
        <p>SHA is a talented graphic designer who has an interest in creating stunning visuals. </p>
          <p>He brings his creative expertise to our platform, ensuring that our templates are not only functional but also visually appealing.</p>
      </div>
    </div>
  );
}

export default About;
