// // // import AllRoutes from './AllRoutes';
// // // import './App.css';
// // // import Navbar from './components/Navbar/Navbar';
// // // import { ToastContainer} from 'react-toastify';
// // // import 'react-toastify/dist/ReactToastify.css';
// // // // import React, { useState } from 'react';
// // // import React from 'react';


// // // // import Routes from './Routes';
// // // function App() {
// // //   return (
// // //     <div className="App">
// // //       <Navbar />
// // //       <ToastContainer />

// // //       <AllRoutes/>
// // //     </div>
// // //   );
// // // }

// // // export default App;

// // import AllRoutes from './AllRoutes';
// // import './App.css';
// // import Navbar from './components/Navbar/Navbar';
// // // import { ToastContainer } from 'react-toastify';
// // // import 'react-toastify/dist/ReactToastify.css';
// // import React  from 'react';

// // function App() {
// //   return (
// //     <div className="App">
// //       <Navbar />
// //       {/* <ToastContainer /> */}

// //       <AllRoutes />
// //     </div>
// //   );
// // }

// // export default App;

// // App.jsx

// // App.js

// // import React, { useState } from 'react';
// // import AllRoutes from './AllRoutes';
// // import Navbar from './components/Navbar/Navbar';
// // function App() {
// //   const [isLoggedIn, setIsLoggedIn] = useState(false);

// //   const handleLogin = () => {
// //     setIsLoggedIn(true);
// //   };

// //   return (
// //     <div>
// //       <Navbar isLoggedIn={isLoggedIn} onLogout={() => setIsLoggedIn(false)} />
// //       <AllRoutes isLoggedIn={isLoggedIn} onLogin={handleLogin} />
// //     </div>
// //   );
// // }

// // export default App;

// import React, { useState } from 'react';
// import AllRoutes from './AllRoutes';
// import Navbar from './components/Navbar/Navbar';

// function App() {
//   const [isLoggedIn, setIsLoggedIn] = useState(false);

//   const handleLogin = () => {
//     setIsLoggedIn(true);
//   };

//   const handleLogout = () => {
//     setIsLoggedIn(false);
//   };

//   return (
//     <div>
//       <Navbar isLoggedIn={isLoggedIn} onLogout={handleLogout} />
//       <AllRoutes isLoggedIn={isLoggedIn} onLogin={handleLogin} />
//     </div>
//   );
// }

// export default App;

// import React, { useState, useEffect } from 'react';
// import AllRoutes from './AllRoutes';
// import Navbar from './components/Navbar/Navbar';
// import Layout from './components/Footer/Layout';
// import './App.css';

// import 'bootstrap/dist/css/bootstrap.min.css';
// import 'codemirror/lib/codemirror.css';
// import 'codemirror/theme/material.css';
// import 'codemirror/mode/stex/stex'; // LaTeX mode

// function App() {
//   const [isLoggedIn, setIsLoggedIn] = useState(false);

//   useEffect(() => {
//     const token = localStorage.getItem('token');
//     if (token) {
//       setIsLoggedIn(true);
//     }
//   }, []);

//   const handleLogin = () => {
//     setIsLoggedIn(true);
//   };

//   const handleLogout = () => {
//     localStorage.removeItem('token');
//     setIsLoggedIn(false);
//   };

//   return (
//     <div className="App">
//       <Navbar isLoggedIn={isLoggedIn} onLogout={handleLogout} />
//       <Layout>
//         <AllRoutes isLoggedIn={isLoggedIn} onLogin={handleLogin} />
//       </Layout>
//     </div>
//   );
// }

// export default App;

import React, { useState, useEffect } from 'react';
import AllRoutes from './AllRoutes';
import Navbar from './components/Navbar/Navbar';
import Layout from './components/Footer/Layout';
import './App.css';

function App() {
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  useEffect(() => {
    const token = localStorage.getItem('token');
    if (token) {
      setIsLoggedIn(true);
    }
  }, []);

  const handleLogin = () => {
    setIsLoggedIn(true);
  };

  const handleLogout = () => {
    localStorage.removeItem('token');
    setIsLoggedIn(false);
  };

  return (
    <div className="App">
      <Navbar isLoggedIn={isLoggedIn} onLogout={handleLogout} />
      <Layout>
        <AllRoutes isLoggedIn={isLoggedIn} onLogin={handleLogin} />
      </Layout>
    </div>
  );
}

export default App;
