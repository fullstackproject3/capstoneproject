class GenerateRandomCreds {
    constructor(name, dob, gender) {
        this.name = name;
        this.dob = dob;
        this.gender = gender;
    }

    randomUserID() {
        const randomNumber = Math.floor(1122 + Math.random() * 8877);
        const nameParts = this.name.split(' ');
        const firstNameInitial = nameParts[0] ? nameParts[0].charAt(0).toUpperCase() : '';
        const lastNameInitial = nameParts.length > 1 ? nameParts[1].charAt(0).toUpperCase() : '';
        const genderInitial = this.gender.charAt(0).toUpperCase();
        return `${firstNameInitial}${lastNameInitial}${genderInitial}${randomNumber}`;
    }

    randomPassword() {
        const chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let password = '';
        for (let i = 0; i < 8; i++) {
            password += chars.charAt(Math.floor(Math.random() * chars.length));
        }
        return password;
    }
}

module.exports = GenerateRandomCreds;
