const mongoose = require('mongoose');

const templateSchema = new mongoose.Schema({
    name: { type: String, required: true },
    description: { type: String, required: true },
    directoryPath: { type: String, required: true },
    imageUrl:{type: String, require: true} // Path to the template directory
});

module.exports = mongoose.model('Template', templateSchema);
