// const mongoose = require('mongoose');

// const userSchema = mongoose.Schema({
//     Name: {
//         type: String,
//         required: [true, "Name is Mandatory"]
//     },
//     DateOfBirth: {
//         type: Date,
//         required: [true, "Date of Birth is Mandatory"]
//     },
//     Email: {
//         type: String,
//         required: [true, "E-Mail is Mandatory"],
//         unique: true,
//         match: [/\S+@\S+\.\S+/, 'Please use a valid email address']
//     },
//     MobileNumber: {
//         type: String,
//         required: [true, "Mobile Number is Mandatory"],
//         unique: true,
//         match: [/^\d{10}$/, 'Please use a valid 10-digit mobile number']
//     },
//     IssuedID: {
//         type: String,
//         required: [true, "Issued ID is Mandatory"],
//         unique: true
//     },
//     Gender: {
//         type: String,
//         enum: ['Male', 'Female', 'Other'],
//         required: [true, "Gender is Mandatory"]
//     }
// });

// const User = mongoose.model('User', userSchema);

// module.exports = User;


const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    Name: {
        type: String,
        required: [true, "Name is Mandatory"]
    },
    DateOfBirth: {
        type: Date,
        required: [true, "Date of Birth is Mandatory"]
    },
    Email: {
        type: String,
        required: [true, "E-Mail is Mandatory"],
        unique: true,
        match: [/\S+@\S+\.\S+/, 'Please use a valid email address']
    },
    MobileNumber: {
        type: String,
        required: [true, "Mobile Number is Mandatory"],
        unique: true,
        match: [/^\d{10}$/, 'Please use a valid 10-digit mobile number']
    },
    IssuedID: {
        type: String,
        required: [true, "Issued ID is Mandatory"],
        unique: true
    },
    Gender: {
        type: String,
        enum: ['Male', 'Female', 'Other'],
        required: [true, "Gender is Mandatory"]
    },
    UserID: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true
    }
});

const User = mongoose.model('User', userSchema);

module.exports = User;
