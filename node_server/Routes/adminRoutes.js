// const express = require('express');
// const jwt = require('jsonwebtoken');
// const router = express.Router();
// const adminAuthMiddleware = require('../middleware/adminAuth');

// router.post('/admin/login', adminAuthMiddleware, (req, res) => {
//     const token = jwt.sign({ username: process.env.ADMIN_USERNAME }, process.env.JWT_SECRET, { expiresIn: '1h' });
//     res.json({ token });
// });

// module.exports = router;

const express = require('express');
const router = express.Router();
const adminAuthMiddleware = require('../middleware/adminAuth');

router.post('/admin/login', adminAuthMiddleware, (req, res) => {
    res.json({ message: 'Login successful', token: req.token });
});

module.exports = router;
