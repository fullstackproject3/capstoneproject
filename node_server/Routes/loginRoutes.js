// const express = require('express');
// const bcrypt = require('bcrypt');
// const User = require('../models/user.model');
// const router = express.Router();

// router.post('/', async (req, res) => { // This should match the route expected by the frontend
//     try {
//         const { username, password } = req.body;

//         // Check if the user exists
//         const user = await User.findOne({ UserID: username }); // Adjust to match the UserID field
//         if (!user) {
//             return res.status(400).json({ message: 'Invalid username or password' });
//         }

//         // Check if the password is correct
//         const isPasswordValid = await bcrypt.compare(password, user.password);
//         if (!isPasswordValid) {
//             return res.status(400).json({ message: 'Invalid username or password' });
//         }

//         // If user and password are valid, return success message
//         res.status(200).json({ message: 'Login successful' });
//     } catch (error) {
//         res.status(500).json({ message: error.message });
//     }
// });

// module.exports = router;
const express = require('express');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const User = require('../models/user.model');
const router = express.Router();
require('dotenv').config();

router.post('/', async (req, res) => {
    try {
        const { username, password } = req.body;

        // Check if the user exists
        const user = await User.findOne({ UserID: username }); // Adjust to match the UserID field
        if (!user) {
            return res.status(400).json({ message: 'Invalid username or password' });
        }

        // Check if the password is correct
        const isPasswordValid = await bcrypt.compare(password, user.password);
        if (!isPasswordValid) {
            return res.status(400).json({ message: 'Invalid username or password' });
        }

        // If user and password are valid, generate a token
        const token = jwt.sign({ username: user.UserID, role: 'user' }, process.env.JWT_SECRET, { expiresIn: '1h' });

        res.status(200).json({ message: 'Login successful', token });
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
});

module.exports = router;
