// // // const express = require('express');
// // // const auth = require('../middleware/auth');
// // // const User = require('../models/user.model');
// // // const router = express.Router();

// // // router.get('/', async (req, res) => {
// // //     try {
// // //         const users = await User.find({});
// // //         res.status(200).json(users);
// // //     } catch (error) {
// // //         res.status(500).json({ message: error.message });
// // //     }
// // // });

// // // router.get('/:id', async (req, res) => {
// // //     try {
// // //         const { id } = req.params;
// // //         const user = await User.findById(id);
// // //         if (!user) {
// // //             return res.status(404).json({ message: 'User not found' });
// // //         }
// // //         res.status(200).json(user);
// // //     } catch (error) {
// // //         res.status(500).json({ message: error.message });
// // //     }
// // // });

// // // router.put('/:id', auth, async (req, res) => {
// // //     try {
// // //         const { id } = req.params;
// // //         const updates = req.body;
// // //         const user = await User.findByIdAndUpdate(id, updates, { new: true, runValidators: true });

// // //         if (!user) {
// // //             return res.status(404).json({ message: 'User not found' });
// // //         }

// // //         res.status(200).json(user);
// // //     } catch (error) {
// // //         res.status(500).json({ message: error.message });
// // //     }
// // // });

// // // router.get('/me', auth, async (req, res) => {
// // //     try {
// // //         const username = req.user.username; // Assuming the username is stored in the JWT
// // //         const user = await User.findOne({ UserID: username });
// // //         if (!user) {
// // //             return res.status(404).json({ message: 'User not found' });
// // //         }
// // //         res.json(user);
// // //     } catch (error) {
// // //         res.status(500).json({ message: error.message });
// // //     }
// // // });

// // // module.exports = router;


// // const express = require('express');
// // const auth = require('../middleware/auth');
// // const User = require('../models/user.model');
// // const router = express.Router();

// // // Get all users
// // router.get('/', async (req, res) => {
// //     try {
// //         const users = await User.find({});
// //         res.status(200).json(users);
// //     } catch (error) {
// //         res.status(500).json({ message: error.message });
// //     }
// // });

// // // Get user by ID
// // router.get('/:id', async (req, res) => {
// //     try {
// //         const { id } = req.params;
// //         const user = await User.findById(id);
// //         if (!user) {
// //             return res.status(404).json({ message: 'User not found' });
// //         }
// //         res.status(200).json(user);
// //     } catch (error) {
// //         res.status(500).json({ message: error.message });
// //     }
// // });

// // // Update user by ID
// // router.put('/:id', auth, async (req, res) => {
// //     try {
// //         const { id } = req.params;
// //         const updates = req.body;
// //         const user = await User.findByIdAndUpdate(id, updates, { new: true, runValidators: true });

// //         if (!user) {
// //             return res.status(404).json({ message: 'User not found' });
// //         }

// //         res.status(200).json(user);
// //     } catch (error) {
// //         res.status(500).json({ message: error.message });
// //     }
// // });

// // // Get authenticated user's info
// // router.get('/me', auth, async (req, res) => {
// //     try {
// //         const username = req.user.username; // Assuming the username is stored in the JWT
// //         const user = await User.findOne({ UserID: username });
// //         if (!user) {
// //             return res.status(404).json({ message: 'User not found' });
// //         }
// //         res.json(user);
// //     } catch (error) {
// //         res.status(500).json({ message: error.message });
// //     }
// // });

// // module.exports = router;

// const express = require('express');
// const auth = require('../middleware/auth');
// const User = require('../models/user.model');
// const router = express.Router();

// // Get all users
// router.get('/', async (req, res) => {
//     try {
//         const users = await User.find({});
//         res.status(200).json(users);
//     } catch (error) {
//         res.status(500).json({ message: error.message });
//     }
// });

// // Get user by ID
// router.get('/:id', async (req, res) => {
//     try {
//         const { id } = req.params;
//         const user = await User.findById(id);
//         if (!user) {
//             return res.status(404).json({ message: 'User not found' });
//         }
//         res.status(200).json(user);
//     } catch (error) {
//         res.status(500).json({ message: error.message });
//     }
// });

// // Update user by ID
// router.put('/:id', auth, async (req, res) => {
//     try {
//         const { id } = req.params;
//         const updates = req.body;
//         const user = await User.findByIdAndUpdate(id, updates, { new: true, runValidators: true });

//         if (!user) {
//             return res.status(404).json({ message: 'User not found' });
//         }

//         res.status(200).json(user);
//     } catch (error) {
//         res.status(500).json({ message: error.message });
//     }
// });

// // Get authenticated user's info
// router.get('/me', auth, async (req, res) => {
//     try {
//         const username = req.user.username; // Assuming the username is stored in the JWT
//         const user = await User.findOne({ UserID: username });
//         if (!user) {
//             return res.status(404).json({ message: 'User not found' });
//         }
//         res.json(user);
//     } catch (error) {
//         res.status(500).json({ message: error.message });
//     }
// });

// module.exports = router;

const express = require('express');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const User = require('../models/user.model');
const Admin = require('../models/admin.model');
const router = express.Router();

// Route to handle user login
router.post('/login', async (req, res) => {
    // Your existing user login code here
});

// Route to handle admin login
router.post('/admin/login', async (req, res) => {
    try {
        const { username, password } = req.body;

        // Check if admin exists
        const admin = await Admin.findOne({ username });
        if (!admin) {
            return res.status(404).json({ message: 'Admin not found' });
        }

        // Validate password
        const isMatch = await bcrypt.compare(password, admin.password);
        if (!isMatch) {
            return res.status(400).json({ message: 'Invalid credentials' });
        }

        // Create and return JWT token
        const payload = {
            admin: {
                id: admin.id,
                username: admin.username
            }
        };

        jwt.sign(payload, process.env.JWT_SECRET, { expiresIn: 3600 }, (err, token) => {
            if (err) throw err;
            res.json({ token });
        });
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
});

module.exports = router;

