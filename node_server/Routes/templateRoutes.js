// const express = require('express');
// const fs = require('fs');
// const path = require('path');
// const multer = require('multer');
// const router = express.Router();
// const Template = require('../models/templates');
// const authenticateJWT = require('../middleware/adminAuth');
// const

// // Set up multer for file uploads
// const storage = multer.diskStorage({
//     destination: (req, file, cb) => {
//         const { name } = req.body;
//         const dir = path.join(__dirname, '..', 'assets', 'LaTexTemplates', name);
//         if (!fs.existsSync(dir)) {
//             fs.mkdirSync(dir, { recursive: true });
//         }
//         cb(null, dir);
//     },
//     filename: (req, file, cb) => {
//         cb(null, file.originalname);
//     }
// });

// const upload = multer({ storage });

// // Create a new template
// router.post('/templates', authenticateJWT, upload.array('files'), async (req, res) => {
//     try {
//         const { name, description } = req.body;
//         const directoryPath = path.join(__dirname, '..', 'assets', 'LaTexTemplates', name);

//         const template = new Template({ name, description, directoryPath });
//         await template.save();
//         res.status(201).send(template);
//     } catch (error) {
//         res.status(400).send(error.message);
//     }
// });

// // Get all templates
// router.get('/templates', async (req, res) => {
//     try {
//         const templates = await Template.find();
//         res.send(templates);
//     } catch (error) {
//         res.status(500).send(error.message);
//     }
// });

// // Get a single template
// router.get('/templates/:id', async (req, res) => {
//     try {
//         const template = await Template.findById(req.params.id);
//         if (!template) return res.status(404).send('Template not found');

//         const files = fs.readdirSync(template.directoryPath).map(filename => {
//             const filePath = path.join(template.directoryPath, filename);
//             const content = fs.readFileSync(filePath, 'utf8');
//             return { filename, content };
//         });

//         res.send({ ...template.toObject(), files });
//     } catch (error) {
//         res.status(500).send(error.message);
//     }
// });

// // Update a template
// router.put('/templates/:id', authenticateJWT, upload.array('files'), async (req, res) => {
//     try {
//         const { name, description } = req.body;
//         const template = await Template.findById(req.params.id);
//         if (!template) return res.status(404).send('Template not found');

//         const directoryPath = path.join(__dirname, '..', 'assets', 'LaTexTemplates', name);

//         // Delete old files and write new ones
//         fs.rmdirSync(template.directoryPath, { recursive: true });
//         req.files.forEach(file => {
//             const filePath = path.join(directoryPath, file.originalname);
//             fs.writeFileSync(filePath, fs.readFileSync(file.path), 'utf8');
//         });

//         template.name = name;
//         template.description = description;
//         template.directoryPath = directoryPath;
//         await template.save();
//         res.send(template);
//     } catch (error) {
//         res.status(400).send(error.message);
//     }
// });

// // Delete a template
// router.delete('/templates/:id', authenticateJWT, async (req, res) => {
//     try {
//         const template = await Template.findById(req.params.id);
//         if (!template) return res.status(404).send('Template not found');

//         fs.rmdirSync(template.directoryPath, { recursive: true });
//         await Template.findByIdAndDelete(req.params.id);
//         res.send({ message: 'Template deleted' });
//     } catch (error) {
//         res.status(500).send(error.message);
//     }
// });

// module.exports = router;

const express = require('express');
const fs = require('fs');
const path = require('path');
const multer = require('multer');
const router = express.Router();
const Template = require('../models/templates');
const authenticateJWT = require('../middleware/authMiddleware');

// Set up multer for file uploads
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        const { name } = req.body;
        const dir = path.join(__dirname, '..', 'assets', 'LaTexTemplates', name);
        if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir, { recursive: true });
        }
        cb(null, dir);
    },
    filename: (req, file, cb) => {
        cb(null, file.originalname);
    }
});

const upload = multer({ storage });

// Create a new template
router.post('/templates', authenticateJWT, upload.array('files'), async (req, res) => {
    try {
        const { name, description } = req.body;
        const directoryPath = path.join(__dirname, '..', 'assets', 'LaTexTemplates', name);

        const template = new Template({ name, description, directoryPath });
        await template.save();
        res.status(201).send(template);
    } catch (error) {
        res.status(400).send(error.message);
    }
});

module.exports = router;
