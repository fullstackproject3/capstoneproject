// // // const express = require('express');
// // // const bcrypt = require('bcrypt');
// // // const User = require('../models/user.model');
// // // const GenerateRandomCreds = require('../utils/generateRandomCreds');
// // // const router = express.Router();

// // // router.post('/register', async (req, res) => {
// // //     try {
// // //         const { Name, DateOfBirth, Email, MobileNumber, IssuedID, Gender } = req.body;

// // //         // Check if the user already exists
// // //         let user = await User.findOne({ Email });
// // //         if (user) {
// // //             return res.status(400).json({ message: 'User already exists with this email' });
// // //         }

// // //         // Generate UserID and password
// // //         const credsGenerator = new GenerateRandomCreds(Name, DateOfBirth, Gender);
// // //         const UserID = credsGenerator.randomUserID();
// // //         const rawPassword = credsGenerator.randomPassword();

// // //         // Hash the password before saving
// // //         const salt = await bcrypt.genSalt(10);
// // //         const hashedPassword = await bcrypt.hash(rawPassword, salt);

// // //         user = new User({ 
// // //             Name, 
// // //             DateOfBirth, 
// // //             Email, 
// // //             MobileNumber, 
// // //             IssuedID, 
// // //             Gender, 
// // //             UserID, 
// // //             password: hashedPassword 
// // //         });
// // //         await user.save();

// // //         res.status(201).json({ 
// // //             message: 'User registered successfully', 
// // //             UserID, 
// // //             rawPassword // Return the generated raw password so the user can see it
// // //         });
// // //     } catch (error) {
// // //         res.status(500).json({ message: error.message });
// // //     }
// // // });

// // // module.exports = router;


// // const express = require('express');
// // const bcrypt = require('bcrypt');
// // const User = require('../models/user.model');
// // const GenerateRandomCreds = require('../utils/generateRandomCreds');
// // const sendEmail = require('../utils/mailer'); // Import the sendEmail function
// // const router = express.Router();

// // router.post('/register', async (req, res) => {
// //     try {
// //         const { Name, DateOfBirth, Email, MobileNumber, IssuedID, Gender } = req.body;

// //         // Check if the user already exists
// //         let user = await User.findOne({ Email });
// //         if (user) {
// //             return res.status(400).json({ message: 'User already exists with this email' });
// //         }

// //         // Generate UserID and password
// //         const credsGenerator = new GenerateRandomCreds(Name, DateOfBirth, Gender);
// //         const UserID = credsGenerator.randomUserID();
// //         const rawPassword = credsGenerator.randomPassword();

// //         // Hash the password before saving
// //         const salt = await bcrypt.genSalt(10);
// //         const hashedPassword = await bcrypt.hash(rawPassword, salt);

// //         user = new User({ 
// //             Name, 
// //             DateOfBirth, 
// //             Email, 
// //             MobileNumber, 
// //             IssuedID, 
// //             Gender, 
// //             UserID, 
// //             password: hashedPassword 
// //         });
// //         await user.save();

// //         // Send email with UserID and raw password
// //         const emailSubject = 'Your Account Details';
// //         const emailText = `Your account has been created. Your UserID is ${UserID} and your password is ${rawPassword}`;
// //         await sendEmail(Email, emailSubject, emailText);

// //         res.status(201).json({ 
// //             message: 'User registered successfully', 
// //             UserID, 
// //             rawPassword // Optionally return the raw password so the user can see it in the response
// //         });
// //     } catch (error) {
// //         res.status(500).json({ message: error.message });
// //     }
// // });

// // module.exports = router;

// const express = require('express');
// const bcrypt = require('bcrypt');
// const User = require('../models/user.model');
// const GenerateRandomCreds = require('../utils/generateRandomCreds');
// const sendEmail = require('../utils/mailer'); // Import the sendEmail function
// const router = express.Router();

// router.post('/register', async (req, res) => {
//     try {
//         const { Name, DateOfBirth, Email, MobileNumber, IssuedID, Gender } = req.body;

//         // Check if the user already exists
//         let user = await User.findOne({ Email });
//         if (user) {
//             return res.status(400).json({ message: 'User already exists with this email' });
//         }

//         // Generate UserID and password
//         const credsGenerator = new GenerateRandomCreds(Name, DateOfBirth, Gender);
//         const UserID = credsGenerator.randomUserID();
//         const rawPassword = credsGenerator.randomPassword();

//         // Hash the password before saving
//         const salt = await bcrypt.genSalt(10);
//         const hashedPassword = await bcrypt.hash(rawPassword, salt);

//         user = new User({ 
//             Name, 
//             DateOfBirth, 
//             Email, 
//             MobileNumber, 
//             IssuedID, 
//             Gender, 
//             UserID, 
//             password: hashedPassword 
//         });
//         await user.save();

//         // Send email with UserID and raw password
//         const emailSubject = 'Your Account Details';
//         const emailText = `Your account has been created. Your UserID is ${UserID} and your password is ${rawPassword}`;
//         await sendEmail(Email, emailSubject, emailText);

//         res.status(201).json({ 
//             message: 'User registered successfully', 
//             UserID, 
//             rawPassword // Optionally return the raw password so the user can see it in the response
//         });
//     } catch (error) {
//         res.status(500).json({ message: error.message });
//     }
// });

// module.exports = router;



const express = require('express');
const bcrypt = require('bcrypt');
const User = require('../models/user.model');
const GenerateRandomCreds = require('../utils/randomCreds');
const sendEmail = require('../utils/mailer'); // Import the sendEmail function
const router = express.Router();

router.post('/register', async (req, res) => {
    try {
        const { Name, DateOfBirth, Email, MobileNumber, IssuedID, Gender } = req.body;

        // Check if the user already exists
        let user = await User.findOne({ Email });
        if (user) {
            return res.status(400).json({ message: 'User already exists with this email' });
        }

        // Generate UserID and password
        const credsGenerator = new GenerateRandomCreds(Name, DateOfBirth, Gender);
        const UserID = credsGenerator.randomUserID();
        const rawPassword = credsGenerator.randomPassword();

        // Hash the password before saving
        const salt = await bcrypt.genSalt(10);
        const hashedPassword = await bcrypt.hash(rawPassword, salt);

        user = new User({ 
            Name, 
            DateOfBirth, 
            Email, 
            MobileNumber, 
            IssuedID, 
            Gender, 
            UserID, 
            password: hashedPassword 
        });
        await user.save();

        // Send email with UserID and raw password
        const emailSubject = 'Your Account Details';
        const emailText = `Your account has been created. Your UserID is ${UserID} and your password is ${rawPassword}`;
        await sendEmail(Email, emailSubject, emailText);

        res.status(201).json({ 
            message: 'User registered successfully', 
            UserID, 
            rawPassword // Optionally return the raw password so the user can see it in the response
        });
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
});

module.exports = router;
